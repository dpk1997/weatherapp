import React from "react";
import { isNull } from "util";

import './Result.css';

const result = props => {

  const f=()=>{
     const arr= JSON.parse(localStorage.getItem('temp'));
     const lastThreeSearch=arr.slice(-3).reverse();
     return(
     
        lastThreeSearch.map(item => {
          return <div className='col-md-3 item text-white m-2 p-2 border' >
            {item.data.name}:<br/>
          {item.data.main.temp} &deg;C 
          </div>;
        })
     
     )

  }

  return (
    <div className="row">
     <div className="col-md-12 text-white text-center">
     Weather Info:{" "}
      {props.error === null ? (isNull(props.temp)?
      (<span>Please search a dropdown option.</span>):(<span>{props.temp} &deg;C ({props.description})  </span> )) 
      : ( props.error)}
      
     </div>
     <div className="col-md-12">
       
     <h4 className="text-white">Previous Search:</h4>
     </div>
     <div className="col-md-12 d-flex flex-row">
      {f()}
     </div>
    </div> 
  );
};

export default result;
