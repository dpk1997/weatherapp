import React from "react";

import "./InputBox.css";

class inputBox extends React.Component {
    state={
        inputBox:'',
        inputPlaceholder:'',
        selectedOption:''
    }
    
    selectListner=(event)=>
    {
        this.setState({
            selectedOption:event.target.value,
            inputPlaceholder:event.target.selectedOptions[0].getAttribute('name'),
            inputBox:''
        })
    }

    onChangeClickListener=(event)=>
    {      
        this.props.apicall(this.state.selectedOption,event.target.value);
        this.setState({
            inputBox:event.target.value
        })
    }
  render() {
    return (
      <div className="row">
          <div className="col-md-4">
        <select onChange={this.selectListner} className="select form-control" name='sd'>
          <option defaultValue > Select</option>
          <option value="q" name='E.g delhi'> City Name</option>
          <option value="zip" name='E.g 94040,Country Code'> Zip Code</option>
          <option value="lat lon" name='E.g 34,113'> Lat/Lon</option>
        </select>
        </div>
        <div className="col-md-8">
        <input
          className="input form-control"
          type="text"
          onChange={this.onChangeClickListener}
          value={this.state.inputBox}
          placeholder={this.state.inputPlaceholder}
        ></input>
        </div>
      </div>
    );
  }
}

export default inputBox;
