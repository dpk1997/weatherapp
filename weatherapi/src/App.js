import React from "react";
import InputBox from "./components/InputBox";
import Result from "./components/Result";
import axios from 'axios';

import "./App.css";



class  App extends React.Component {

  state=({
    temp:null,
    description:null,
    error:null
  });



  appid='76ce088067e396b4fc0051aec7cc6db4&units=metric';
  baseUrl='https://api.openweathermap.org/data/2.5/weather?';


   arr=[];
  localStore=()=>
  {
      localStorage.setItem('temp',JSON.stringify(this.arr));
  
  }

   call=(link)=>
  {
    axios.get(link)
      .then(response=>{
        this.arr=JSON.parse(localStorage.getItem('temp'))
       this.arr.push(response); 
       this.localStore();
        this.setState({
         temp:response.data.main.temp,  
         description:response.data.weather[0].description,
          error:null
        })
       
      })
      .catch(
        reject  => {
          if(reject.response){
            console.log(reject.response.data.message);
            this.setState({
              temp:null,
              description:null,
              error:reject.response.data.message
            })
          }
         
        }) 

  }


  
  inputHandler =  (selectedOption,inputValue) => {
   let link='';
    switch(selectedOption)
    {
      case 'q':      
         link =this.baseUrl+selectedOption+'='+inputValue+'&appId='+this.appid;
        break;
      case 'zip':
        let [zip,countryCode='']=inputValue.split(',');
          link =this.baseUrl+selectedOption+'='+zip+','+countryCode+'&appId='+this.appid;
        break;
      case 'lat lon':
         let[latv ,lonv]=inputValue.split(',');
         let[lat ,lon]=selectedOption.split(' ');
         link =this.baseUrl+lat+'='+latv+'&'+lon+'='+lonv+'&appId='+this.appid;
        break;

      default:

 
    }
    this.call(link);
    }
   


render()
{
  return (
    
  <div className="container mt-4">
  <div className="row">
    
    <div className="col-md-3">
  
    </div>
    <div className="col-md-6">
    <div className="App bg-primary  ">
    <InputBox apicall={this.inputHandler}/>
    <br/>
    <Result temp ={this.state.temp} description={this.state.description} error={this.state.error}/>
  </div>
    </div>
    <div className="col-md-3">
  
    </div>
  </div>
    </div>
    
  );

}
 
}

export default App;
